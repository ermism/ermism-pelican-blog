#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Ermis Mage'
SITENAME = "Eventual Ideas"
SITEURL = 'https://ermism.bitbucket.io'
SITESUBTITLE = 'Thinking Taken Seriously'

PATH = 'content'
OUTPUT_PATH = 'public' # also in Makefile

THEME = 'themes/attila'
#THEME = 'themes/html5-dopetrope'

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = 'en'

DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%a %d %b %Y %H:%M:%S'
ARTICLE_ORDER_BY = 'reversed-modified'


#SUMMARY_MAX_LENGTH = None

#MARKUP = ('md', 'ipynb')
MARKUP = ('md')

DISPLAY_PAGES_ON_MENU = True

PLUGIN_PATHS = [
    'plugins/'
]

PLUGINS = [
    #'just_table',
    'liquid_tags.youtube',
    #'liquid_tags.notebook',
    #'ipynb.markup',
    'pin_to_top',
    #'post_revision',
    #'render_math',
    #'rmd_reader',
    'show_source',
    # 'summary',
]

# Feed generation is usually not desired when developing
#FEED_ALL_ATOM = 'feeds/all.atom.xml'
# CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
#TRANSLATION_FEED_ATOM = None
#AUTHOR_FEED_ATOM = None
#AUTHOR_FEED_RSS = None

# for post_revision
#GITHUB_URL = 'https://bitbucket.com/ermism/ermism.bitbucket.io'
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

SHOW_FULL_ARTICLE = True

RMD_READER_KNITR_OPTS_CHUNK = {
    'class.output': 'knitr-output',
    'collapse': True,
    'comment': '',
    # 'prompt': True,        
    'results': 'asis',
}

RMD_READER_KNITR_OPTS_KNIT = None

MARKDOWN = {
        'extensions_configs':
            {
                'markdown.extensions.extra': {},
                'markdown.extensions.meta': {},
                'markdown.extensions.toc': {},
                'markdown.extensions.attr_list': {},
            },
        'extensions': ['extra', 'meta', 'toc', 'attr_list']

        }

MATH_JAX = {
    'message_style': None,
}

SHOW_SOURCE_IN_SECTION = True
SHOW_SOURCE_ALL_POSTS = True

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('bitbucket','https://bitbucket.com/ermism/'),)

DEFAULT_PAGINATION = 17

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

AUTHORS_BIO = {
  "Ermis Mage": {
    "name": "ERMiS",
    "cover": "https://bitbucket.com/uploads/-/system/user/avatar/4872500/avatar.png?width=700",
    "image": "https://bitbucket.com/uploads/-/system/user/avatar/4872500/avatar.png?width=400",
    "website": "https://ermism.bitbucket.io",
    "location": "Own Space",
    "bio": "Living and improving myself!"
  }
}

GOOGLE_ANALYTICS = "UA-100408965-1"


